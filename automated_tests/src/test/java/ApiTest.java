import io.restassured.http.ContentType;
import org.junit.Test;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertTrue;

public class ApiTest {

    @Test
    public void shouldReturnStatus200AndValidateNameUserNameAndEmailAllUsers() {
        given()
               .get("https://jsonplaceholder.typicode.com/users/")
                .then()
                .statusCode(200)
                .body("name", notNullValue())
                .body("username", notNullValue())
                .body("email", notNullValue())
                .log().body();
    }

    @Test
    public void shouldReturnStatus200AndValidateEmailPattern() {
        given()
                .contentType(ContentType.JSON)
                .get("https://jsonplaceholder.typicode.com/users/")
                .then()
                .statusCode(200)
                .body("email", everyItem(containsString("@")))
                .log().body();
    }

    @Test
    public void shouldReturnStatus200AndValidateCatchPhraseLenghtLessThanFifty() {
        List<String> catchPhrases = given()
                .contentType(ContentType.JSON)
                .when()
                .get("https://jsonplaceholder.typicode.com/users/")
                .then()
                .statusCode(200)
                .extract()
                .response()
                .body().path("company.catchPhrase");

        catchPhrases.forEach(cp -> assertTrue(cp.length() < 50));
    }

    @Test
    public void shouldReturnStatus201WhenCreatingNewPost(){
        String userId = given()
                .contentType(ContentType.JSON)
                .when()
                .get("https://jsonplaceholder.typicode.com/users/")
                .body().path("[0].id").toString();

        Map<String, String> newPost = new HashMap<>();

        newPost.put("userId", userId);
        newPost.put("title", "My name is Lara");
        newPost.put("body", "i'm 25");

        given()
                .contentType(ContentType.JSON)
                .when()
                .body(newPost)
                .post("https://jsonplaceholder.typicode.com/posts")
                .then()
                .statusCode(201)
                .body("userId", equalTo("1"))
                .log().body();
    }

    @Test
    public void shouldReturnStatus400WhenCreatingNewPostWithoutTheTitle(){
        String userId = given()
                .contentType(ContentType.JSON)
                .when()
                .get("https://jsonplaceholder.typicode.com/users/")
                .body().path("[0].id").toString();

        Map<String, String> newPost = new HashMap<>();

        newPost.put("userId", userId);
        newPost.put("body", "i'm study");

       given()
                .contentType(ContentType.JSON)
                .when()
                .body(newPost)
                .post("https://jsonplaceholder.typicode.com/posts")
                .then()
                .statusCode(400);

    }
}
